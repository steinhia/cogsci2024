# Cogsci2024

This code is the code of the result analysis of the paper "Comparing online and post-processing pronunciation correction during orthographic incidental learning: A computational study in the BRAID-Acq model" published in the Cognitive Science Conference 2024.

The model used to run the simulations can be found on: https://gricad-gitlab.univ-grenoble-alpes.fr/diardj/braidk